package xyz.herex.sprookjescraft.hotel.hotelsc.room;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import xyz.herex.sprookjescraft.hotel.hotelsc.HotelSC;
import xyz.herex.sprookjescraft.hotel.hotelsc.filemanager.Config;
import xyz.herex.sprookjescraft.hotel.hotelsc.filemanager.Configs;

import java.io.File;
import java.util.*;

/**
 * Created by: Maurice Beerenfenger
 * Buisniss: HerexDevelopment
 * Created on: 2018/08/28
 * Project: HotelSC
 */
public class RoomManager {

    private List<Room> rooms = new ArrayList<>(); // Goed gebruik van polymorfie. Hiermee kan je later bijv. overstappen naar LinkedList zonder andere code aan te passen
    private Map<UUID, Room> confirmcache = new HashMap<>(); // Ook hier kan je confirmcache op interface niveau gebruiken zoals je bij 'rooms' doet.
    public RoomManager(){
        loadRooms();
    }


    public Room playerGetRoom(Player p){
        for(Room r : rooms){
            if(r.getOwner().equalsIgnoreCase(p.getUniqueId().toString())){
                return r;
            }
        }
        return null;
    }


    public Room getfromCache(Player player){
        for (Map.Entry<UUID, Room> rooms : confirmcache.entrySet()){
            if(rooms.getKey() == player.getUniqueId()){
                return rooms.getValue();
            }
        }
        return null;
    }

    // Als je niet van plan bent om dit te gaan gebruiken, haal het dan gerust weg.
    // YAGNI! http://www.extremeprogramming.org/rules/early.html
    public Room getfromCache(UUID player){
        for (Map.Entry<UUID, Room> rooms : confirmcache.entrySet()){
            if(rooms.getKey() == player){
                return rooms.getValue();
            }
        }
        return null;
    }

    public Map<UUID, Room> getConfirmcache() {
        return confirmcache;
    }

    private void loadRooms(){
        // Je gebruik van variabelen is hier onduidelijk aangezien de 'i' die je defined in de for-loop meestal wordt gebruikt en 'is' een duidelijkere naamgeving kan krijgen
        // Dus precies andersom:
        /**
         * int maxRooms = 5;
         * for(int i = 0; i < maxRooms; i++){
         */
        int is = 0;
        for(int i = 5; is < i; is++){
            Config c = new Config("Standaard-" + is, new File(HotelSC.getInstance().getDataFolder() + "/rooms/")); // Elke kamer heeft een eigen config, top. Dat is goed voor parallele taken (als er iets aangepast moet worden in 2 kamers op precies hetzelfde moment).
            if( c.getConfig().getString("naam") == null || c.getConfig().getString("naam").equalsIgnoreCase("")){
                List k = new ArrayList<String>(); // Wat is k?
                c.getConfig().set("naam","Standaard-" + is );
                c.getConfig().set("houder","");
                c.getConfig().set("loc1", "");
                c.getConfig().set("loc2", "");
                c.getConfig().set("rented", false);
                c.getConfig().set("friends", k); // c.getConfig().set("friends", new ArrayList<String>());
                c.saveConfig();
                Configs.getInstance().put("Standaard-" + is, c);
                List<String> friends; // Wat kan efficiënter?
                friends = c.getConfig().getStringList("friends"); // Wat kan efficiënter?
                rooms.add(new Room(c.getConfig().getString("houder"), c.getConfig().getString("naam"), null, c.getConfig().getBoolean("rented"), friends));
            }else {
                if(c.getConfig().getConfigurationSection("loc1") == null){
                    List<String> friends; // Wat kan efficiënter?
                    friends = c.getConfig().getStringList("friends"); // Wat kan efficiënter?
                    rooms.add(new Room(c.getConfig().getString("houder"), c.getConfig().getString("naam"), null, c.getConfig().getBoolean("rented"), friends));

                }else {
                    Location[] locations = new Location[2];
                    locations[0] = new Location(Bukkit.getWorld(c.getConfig().getString("loc1.world")), c.getConfig().getDouble("loc1.x"), c.getConfig().getDouble("loc1.y"), c.getConfig().getDouble("loc1.z"));
                    locations[1] = new Location(Bukkit.getWorld(c.getConfig().getString("loc2.world")), c.getConfig().getDouble("loc2.x"), c.getConfig().getDouble("loc2.y"), c.getConfig().getDouble("loc2.z"));
                   List<String> friends; // Wat kan efficiënter?
                   friends = c.getConfig().getStringList("friends"); // Wat kan efficiënter?
                    rooms.add(new Room(c.getConfig().getString("houder"), c.getConfig().getString("naam"), locations, c.getConfig().getBoolean("rented"), friends));
                }
            }
            // In alle codepaden roep je rooms.add() aan waarbij in één geval 'locations' gevuld is, en in de andere gevallen de waarde null heeft.
            // Je zou 'locations' als null bovenaan in deze for-loop kunnen zetten met de null value en in de laatste else geef je 'm new Location[2];

            // En je kan hier List<String> friends = c.getConfig().getStringList("friends");
            // Tenslotte hier rooms.add() aanroepen

            // Scheelt weer een paar regels dubbele code
        }
    }

    public Room getRoombyName(String naam){
        for(Room r : rooms){
            if(r.getRoomLabel().equalsIgnoreCase(naam)){
                return r;
            }

        }
        return null;
    }

    public List<Room> getRooms() {
        return rooms;
    }
}
