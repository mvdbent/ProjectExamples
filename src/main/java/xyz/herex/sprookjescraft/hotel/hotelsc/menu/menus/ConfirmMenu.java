package xyz.herex.sprookjescraft.hotel.hotelsc.menu.menus;

import org.bukkit.Material;
import org.bukkit.inventory.InventoryHolder;
import xyz.herex.sprookjescraft.hotel.hotelsc.HotelSC;
import xyz.herex.sprookjescraft.hotel.hotelsc.filemanager.Config;
import xyz.herex.sprookjescraft.hotel.hotelsc.filemanager.Configs;
import xyz.herex.sprookjescraft.hotel.hotelsc.menu.Gui;
import xyz.herex.sprookjescraft.hotel.hotelsc.menu.GuiType;
import xyz.herex.sprookjescraft.hotel.hotelsc.menu.InventorySize;
import xyz.herex.sprookjescraft.hotel.hotelsc.room.Room;
import xyz.herex.sprookjescraft.hotel.hotelsc.utils.C;
import xyz.herex.sprookjescraft.hotel.hotelsc.utils.ItemBuilder;

import java.io.File;

/**
 * Created by: Maurice Beerenfenger
 * Buisniss: HerexDevelopment
 * Created on: 2018/08/30
 * Project: HotelSC
 */
public class ConfirmMenu extends Gui {
    public ConfirmMenu() {
        super("Weet je het zeker?", InventorySize.TWENTYSEVEN, GuiType.NORMAL, null);
          addItem(12, new ItemBuilder(Material.STAINED_CLAY).withData((byte)5).withDisplayname("&aIk weet het zeker!").build(), (player, clickType) -> {
            Room r = HotelSC.getInstance().getRoomManager().getfromCache(player);
            r.setRented(false);
            r.setOwner("");
            Config c =  new Config(r.getRoomLabel(), new File(HotelSC.getInstance().getDataFolder() + "/rooms/"));
            c.getConfig().getStringList("friends").clear();
            c.saveConfig();
            player.sendMessage(C.K("&9[Hotel]&f Huur is gestopt!"));
            player.closeInventory();
        });

        addItem(14, new ItemBuilder(Material.STAINED_CLAY).withData((byte)14).withDisplayname("&cNee, dit klopt niet!").build(), (player, clickType) -> {
            player.closeInventory();
        });
    }
}
