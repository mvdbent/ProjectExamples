package xyz.herex.sprookjescraft.hotel.hotelsc.menu;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import xyz.herex.sprookjescraft.hotel.hotelsc.HotelSC;
import xyz.herex.sprookjescraft.hotel.hotelsc.menu.menus.ConfirmMenu;
import xyz.herex.sprookjescraft.hotel.hotelsc.menu.menus.RoomMenu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by: Maurice Beerenfenger
 * Buisniss: HerexDevelopment
 * Created on: 2018/08/28
 * Project: HotelSC
 */
public class GuiManager implements Listener {
    public List<Gui> inventorys = new ArrayList<>();
    public GuiManager(){
        Bukkit.getPluginManager().registerEvents(this, HotelSC.getInstance());

        registerGUI(new RoomMenu());
        registerGUI(new ConfirmMenu());
        // Je roept heel netjes andere methodes aan om vervolgens het werk te doen, zo hoort dat in een constructor.
    }

    private void registerGUI(Gui gui){
        if(!inventorys.contains(gui)){
            inventorys.add(gui);
        }

    }
    @EventHandler
    public void onClick(InventoryClickEvent e){
        if(e.getCurrentItem() == null){
            e.setCancelled(true);
        }

        if(e.getInventory() == null){
            e.setCancelled(true);
        }
        // Kan je deze twee if-statements in één doen? Scheelt weer 4 regels. Return dan ook gelijk zodat je de for-loops niet bereikt.

        for(Gui inv : inventorys) {
            for (GuiComponent c : inv.getItems()) {
                if(e.getInventory().getName().equalsIgnoreCase(inv.getName())){
                    if (c.getItemStack().isSimilar(e.getCurrentItem())) {
                        c.getOnClick().accept((Player) e.getWhoClicked(), e.getClick());
                        e.setCancelled(true);
                        // Return hier, dat scheelt je veel performance als we veel GUI's en items hebben
                    }
                }
            }
        }
    }
}
