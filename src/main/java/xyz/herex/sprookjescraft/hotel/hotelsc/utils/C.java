package xyz.herex.sprookjescraft.hotel.hotelsc.utils;

import net.md_5.bungee.api.ChatColor;

/**
 * Created by: Maurice Beerenfenger
 * Buisniss: HerexDevelopment
 * Created on: 2018/08/28
 * Project: HotelSC
 */
public class C {

    // In dit geval kan ik begrijpen dat je afkortingen gebruikt omdat het dan erg
    // lang wordt bij sommige messages, maar probeer het afkorten van variabelen,
    // classes en methoden te voorkomen omdat je "collega's" er dan niets van snappen
    public static String  K(String input){
        return ChatColor.translateAlternateColorCodes('&', input);
    }
}
