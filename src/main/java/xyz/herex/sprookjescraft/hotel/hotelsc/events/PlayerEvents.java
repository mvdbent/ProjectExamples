package xyz.herex.sprookjescraft.hotel.hotelsc.events;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import xyz.herex.sprookjescraft.hotel.hotelsc.HotelSC;
import xyz.herex.sprookjescraft.hotel.hotelsc.room.Room;
import xyz.herex.sprookjescraft.hotel.hotelsc.utils.C;

import java.util.Arrays;

/**
 * Created by: Maurice Beerenfenger
 * Buisniss: HerexDevelopment
 * Created on: 2018/08/29
 * Project: HotelSC
 */
public class PlayerEvents implements Listener {

    @EventHandler
    public void onBuild(PlayerInteractEvent e){
        if(e.getAction() == Action.RIGHT_CLICK_BLOCK){
        // De onderstaande if kan wat overzichtelijker. Gooi al die DOOR materials in een array en gebruik de contains methode van die array.
        if(e.getClickedBlock().getType() == Material.WOODEN_DOOR || e.getClickedBlock().getType() == Material.WOOD_DOOR || e.getClickedBlock().getType() == Material.JUNGLE_DOOR || e.getClickedBlock().getType() == Material.DARK_OAK_DOOR ||
                e.getClickedBlock().getType() == Material.BIRCH_DOOR || e.getClickedBlock().getType() == Material.ACACIA_DOOR || e.getClickedBlock().getType() == Material.SPRUCE_DOOR ){
            Room m = HotelSC.getInstance().getRoomManager().playerGetRoom(e.getPlayer()); // Variable naam kan duidelijker
            if(m != null) {
                if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    if (isInRect(e.getClickedBlock(), m.getCornerLocations()[1], m.getCornerLocations()[0])) {
                        e.setCancelled(false);
                    } else {
                        e.setCancelled(true);
                        e.getPlayer().sendMessage(C.K("&9[Hotel] &fDit is niet jouw hotel deur!"));
                    }
                }
                for(Room ms : HotelSC.getInstance().getRoomManager().getRooms()){
                    if(ms.getCornerLocations() != null) {
                        if (isInRect(e.getClickedBlock(), ms.getCornerLocations()[1], ms.getCornerLocations()[0]) && ms.getInvitedplayers().contains(e.getPlayer().getUniqueId().toString())) {
                            e.setCancelled(false) ;
                        }
                    }
                    }
                }else if(e.getPlayer().hasPermission("hotel.rooms.accessall") || m.getInvitedplayers().contains(e.getPlayer().getUniqueId().toString())){ // getInvitedplayers() kan null returnen
                    e.setCancelled(false);

            }else{
                e.setCancelled(true);
                e.getPlayer().sendMessage(C.K("&9[Hotel] &fDit is niet jouw hotel deur!"));
            }
        }
            }
        }

    // Duidelijke methodenaam
    private boolean isInRect(Block b, Location loc1, Location loc2) {
        double[] dim = new double[2];

        dim[0] = loc1.getX();
        dim[1] = loc2.getX();
        Arrays.sort(dim);
        if (b.getLocation().getX() > dim[1] || b.getLocation().getX() < dim[0])
            return false;

        dim[0] = loc1.getZ();
        dim[1] = loc2.getZ();
        Arrays.sort(dim);
        if (b.getLocation().getZ() > dim[1] || b.getLocation().getZ() < dim[0])
            return false;
        dim[0] = loc1.getY();
        dim[1] = loc2.getY();
        Arrays.sort(dim);
        if (b.getLocation().getY() > dim[1] || b.getLocation().getY() < dim[0])
            return false;

        return true;
    }

}
