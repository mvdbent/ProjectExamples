package com.mortalpvp.mpkingdomdungeon.builders.mobuilder;

public class MobDefaultSettings {

    private double dealthDamageonHit;
    private double speed;
    private boolean spinningOnHit;
    private int followrange;

    public MobDefaultSettings(double dealthDamageonHit, double speed, boolean spinningOnHit, int followrange) {
        this.dealthDamageonHit = dealthDamageonHit;
        this.speed = speed;
        this.spinningOnHit = spinningOnHit;
        this.followrange = followrange;
    }

    public double getDealthDamageonHit() {
        return dealthDamageonHit;
    }

    public void setDealthDamageonHit(double dealthDamageonHit) {
        this.dealthDamageonHit = dealthDamageonHit;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public boolean isSpinningOnHit() {
        return spinningOnHit;
    }

    public void setSpinningOnHit(boolean spinningOnHit) {
        this.spinningOnHit = spinningOnHit;
    }

    public int getFollowrange() {
        return followrange;
    }

    public void setFollowrange(int followrange) {
        this.followrange = followrange;
    }
}
