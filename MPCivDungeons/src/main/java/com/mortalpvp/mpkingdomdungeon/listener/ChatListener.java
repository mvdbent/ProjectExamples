package com.mortalpvp.mpkingdomdungeon.listener;

import com.mortalpvp.core.api.MortalAPI;
import com.mortalpvp.core.api.plugin.listener.MortalListener;
import com.mortalpvp.mpkingdomdungeon.Main;
import com.mortalpvp.mpkingdomdungeon.manager.editmanager.EditPlayer;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener extends MortalListener<Main> {
    public ChatListener(Main plugin, MortalAPI api) {
        super(plugin, api);
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        if (Main.getPlugin(Main.class).getDungeonManager().getInEditing().stream()
                .anyMatch(m -> m.getUuid() == e.getPlayer().getUniqueId()) && e.getMessage().equalsIgnoreCase("ok")) {
            EditPlayer d = Main.getPlugin(Main.class).getDungeonManager().getInEditing().stream()
                    .filter(m -> m.getUuid() == e.getPlayer().getUniqueId()).findAny().get();
            Main.getPlugin(Main.class).getEditManager().handleRequest(d, Bukkit.getPlayer(d.getUuid()), d.getEditType(),
                    Main.getPlugin(Main.class).getWorldEditPlugin());
            Main.getPlugin(Main.class).getDungeonManager().getInEditing().remove(d);
        }
    }
}
