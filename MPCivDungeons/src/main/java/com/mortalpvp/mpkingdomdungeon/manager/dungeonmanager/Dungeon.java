package com.mortalpvp.mpkingdomdungeon.manager.dungeonmanager;

import com.mortalpvp.mpkingdomdungeon.Main;
import com.mortalpvp.mpkingdomdungeon.manager.editmanager.EditPlayer;
import com.mortalpvp.mpkingdomdungeon.manager.filemanager.Config;
import org.apache.commons.collections.MultiHashMap;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Dungeon {
    private String name, world;
    private int bosid, level;
    private Location bosslocation;
    private List<Location> portlocations;
    private List<ItemStack> rewarditems;
    private HashMap<Location, Integer> stages, mobspawnpoints;
    private Location[] arenalocs;
    //TODO: HOLOGRAM

    public Dungeon(String name, String world, int bosid, Location bosslocation, List<Location> portlocations, List<ItemStack> rewarditems, HashMap<Location, Integer> stages, HashMap<Location, Integer> mobspawnpoints, int level, Location[] arenalocs) {
        this.name = name;
        this.world = world;
        this.bosid = bosid;
        this.bosslocation = bosslocation;
        this.portlocations = portlocations;
        this.rewarditems = rewarditems;
        this.stages = stages;
        this.mobspawnpoints = mobspawnpoints;
        this.level = level;
        this.arenalocs = arenalocs;

    }


    public Location[] getArenalocs() {
        return arenalocs;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorld() {
        return world;
    }

    public void setWorld(String world) {
        this.world = world;
    }

    public int getBosid() {
        return bosid;
    }

    public void setBosid(int bosid) {
        this.bosid = bosid;
    }

    public Location getBosslocation() {
        return bosslocation;
    }

    public void setBosslocation(Location bosslocation) {
        this.bosslocation = bosslocation;
    }

    public List<Location> getPortlocations() {
        return portlocations;
    }

    public boolean isInRect(Player player, Location loc1, Location loc2) {
        double[] dim = new double[2];

        dim[0] = loc1.getX();
        dim[1] = loc2.getX();
        Arrays.sort(dim);
        if (player.getLocation().getX() > dim[1] || player.getLocation().getX() < dim[0])
            return false;

        dim[0] = loc1.getZ();
        dim[1] = loc2.getZ();
        Arrays.sort(dim);
        if (player.getLocation().getZ() > dim[1] || player.getLocation().getZ() < dim[0])
            return false;
        dim[0] = loc1.getY();
        dim[1] = loc2.getY();
        Arrays.sort(dim);
        if (player.getLocation().getY() > dim[1] || player.getLocation().getY() < dim[0])
            return false;

        return true;
    }

    public boolean walksBorder(Player p, Location loc1, Location loc2) {
        double x = p.getLocation().getX();
        double y = p.getLocation().getY();
        double z = p.getLocation().getZ();
        int maxX = loc1.getBlockX();
        int minX = loc2.getBlockX();
        int maxZ = loc1.getBlockZ();
        int minZ = loc2.getBlockZ();
        int minY = loc1.getBlockY();
        int maxY = loc2.getBlockY();
        Math.sqrt(Math.pow((minX - maxX), 2) + Math.pow(minZ - maxY, 2));

        return false;
    }



    public void setPortlocations(EditPlayer editPlayer, List<Location> portlocations) {
        for(Config cf : Main.getInstance().getDungeonManager().getConfigs()){
            if(cf.getConfig().getString("settings.name").equalsIgnoreCase(getName())){

                cf.getConfig().set("selection.port.posx", portlocations.get(0).getX());
                cf.getConfig().set("selection.port.posy", portlocations.get(0).getY());
                cf.getConfig().set("selection.port.posz", portlocations.get(0).getZ());


                cf.getConfig().set("selection.port.pos2x", portlocations.get(1).getX());
                cf.getConfig().set("selection.port.pos2y", portlocations.get(1).getY());
                cf.getConfig().set("selection.port.pos2z", portlocations.get(1).getZ());
                cf.saveConfig();

            }
        }
            this.portlocations = portlocations;
    }

    public List<ItemStack> getRewarditems() {
        return rewarditems;
    }

    public void setRewarditems(List<ItemStack> rewarditems) {
        this.rewarditems = rewarditems;
    }

    public HashMap<Location, Integer> getStages() {
        return stages;
    }

    public void setStages(HashMap<Location, Integer> stages) {
        this.stages = stages;
    }

    public HashMap getMobspawnpoints() {
        return mobspawnpoints;
    }

    public void setMobspawnpoints(MultiHashMap mobspawnpoints) {
        this.mobspawnpoints = mobspawnpoints;
    }
}
