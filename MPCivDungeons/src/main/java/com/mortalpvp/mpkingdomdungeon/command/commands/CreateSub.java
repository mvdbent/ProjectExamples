package com.mortalpvp.mpkingdomdungeon.command.commands;

import com.mortalpvp.core.api.command.command.MortalCommand;
import com.mortalpvp.core.api.command.command.SubCommand;
import com.mortalpvp.core.api.command.data.Argument;
import com.mortalpvp.core.api.command.data.CommandArgument;
import com.mortalpvp.core.api.language.Translatable;
import com.mortalpvp.core.bukkit.util.ItemCreator;
import com.mortalpvp.mpkingdomdungeon.Main;
import com.mortalpvp.mpkingdomdungeon.builders.mobuilder.MobBuilder;
import com.mortalpvp.mpkingdomdungeon.builders.mobuilder.MobDefaultSettings;
import com.mortalpvp.mpkingdomdungeon.ui.builderui.BuilderHome;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class CreateSub extends SubCommand {
    private final Main plugin;

    public CreateSub(Main plugin, MortalCommand baseCommand) {
        super(baseCommand, "create", new Translatable().append("blaad"), "");

        this.plugin = plugin;
    }

    @Override
    public List<String> getAliases() {
        return null;
    }

    @Override
    public List<Argument> getArguments() {
        return null;
    }

    @Override
    public List<SubCommand> getSubCommands() {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, String s, List<CommandArgument> list) {
        if (commandSender instanceof Player) {
            Player p = (Player) commandSender;
            plugin.getItemCreatorManager().getItemQueue().put(p.getUniqueId(),
                    new ItemCreator(Material.PAPER).setDisplayName("Im a default item").Build());
            MobBuilder builder = new MobBuilder(new MobDefaultSettings(0.5, 0.2, false, 2));
            new BuilderHome(builder, plugin).open(p);
        }
        return true;
    }

    @Override
    public void onError(CommandSender commandSender, Exception e) {

    }

}
