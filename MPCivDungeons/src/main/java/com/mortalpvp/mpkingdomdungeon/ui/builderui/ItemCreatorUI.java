package com.mortalpvp.mpkingdomdungeon.ui.builderui;

import com.mortalpvp.core.api.ui.UI;
import com.mortalpvp.core.api.ui.components.UIComponent;
import com.mortalpvp.core.bukkit.util.ItemCreator;
import com.mortalpvp.core.common.chat.C;
import com.mortalpvp.mpkingdomdungeon.Main;
import com.mortalpvp.mpkingdomdungeon.builders.mobuilder.MobBuilder;
import com.mortalpvp.mpkingdomdungeon.ui.builderui.itemcreator.ItemLoreUI;
import com.mortalpvp.mpkingdomdungeon.utils.inputgui.AnvilGUI;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemCreatorUI extends UI {
    public ItemCreatorUI(MobBuilder builder, ItemStack stack, WichMenu wichMenu, Main plugin) {

        super(plugin, "ItemBuilder", 9 * 6, InventoryType.CHEST);
        UIComponent border = new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                ItemStack i = new ItemCreator(Material.STAINED_GLASS_PANE).setDurability((short) 10).Create();
                return i;
            }
        };

        UIComponent previtem = new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return plugin.getItemCreatorManager().getStackFromQueue(player);
            }
        };
        setComponent(0, border);
        setComponent(1, border);
        setComponent(2, border);
        setComponent(3, border);
        setComponent(4, border);
        setComponent(5, border);
        setComponent(6, border);
        setComponent(7, border);
        setComponent(8, border);
        setComponent(9, border);
        setComponent(17, border);
        setComponent(18, border);
        setComponent(26, border);
        setComponent(27, border);
        setComponent(35, border);
        setComponent(36, border);
        setComponent(44, border);
        setComponent(45, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.ARROW).setDisplayName("&6Back").Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                new BuilderHome(builder, plugin).open(player);
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_HARP, 1, 1);
            }
        }));
        setComponent(46, border);
        setComponent(47, border);
        setComponent(48, border);
        setComponent(49, border);
        setComponent(50, border);
        setComponent(51, border);
        setComponent(52, border);
        setComponent(53, border);
        setComponent(4, previtem);
        setComponent(8, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.EMERALD_BLOCK).setDisplayName("&aFinish").Create();
            }
        });

        setComponent(20, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.BOOK_AND_QUILL).setDisplayName("&6Change displayname").Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                new AnvilGUI(plugin, player, "Type a name", ((p, output) -> {
                    ItemStack newitem = previtem.getItemStack(player);

                    ItemMeta meta = newitem.getItemMeta();
                    meta.setDisplayName(C.TAC(output));
                    newitem.setItemMeta(meta);
                    player.sendMessage(C.TAC("&7[&5Dungeons&7] &7Changed displayname to: " + output));
                    player.playSound(player.getLocation(), Sound.BLOCK_NOTE_HARP, 1, 1);
                    plugin.getItemCreatorManager().ApplayChanges(player, stack, newitem);
                    new ItemCreatorUI(builder, newitem, wichMenu, plugin).open(p);
                    return null;
                }), builder);
            }
        }));

        setComponent(22, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.DIAMOND).setDisplayName("&6 Add/Remove lores").Create();
            }
        }.onInteract((player, clickType) -> {
            if (clickType.isLeftClick()) {
                new ItemLoreUI(plugin, builder, player, wichMenu).open(player);
            }
        }));
    }
}
