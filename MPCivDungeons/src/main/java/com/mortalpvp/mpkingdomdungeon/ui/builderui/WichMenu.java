package com.mortalpvp.mpkingdomdungeon.ui.builderui;

public enum WichMenu {

    LOOTITEMS("LootItems");

    private final String text;
    WichMenu(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
