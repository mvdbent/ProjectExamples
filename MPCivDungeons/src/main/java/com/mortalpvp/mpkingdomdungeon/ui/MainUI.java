package com.mortalpvp.mpkingdomdungeon.ui;

import com.mortalpvp.core.api.ui.UI;
import com.mortalpvp.core.api.ui.components.UIComponent;
import com.mortalpvp.core.bukkit.util.ItemCreator;
import com.mortalpvp.mpkingdomdungeon.Main;
import com.mortalpvp.mpkingdomdungeon.manager.dungeonmanager.Dungeon;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

public class MainUI extends UI {

    public MainUI(Main plugin) {
        super(plugin, "Dungeons", 5 * 9, InventoryType.CHEST/*, new int[]{10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34}*/);

        /* setPreviousArrowSlot(38);
        setNextArrowSlot(40); */

        plugin.getDungeonManager().getDungeons().forEach(dungeon -> {
            addComponent(getUIComponent(dungeon));
        });
    }

    private UIComponent getUIComponent(Dungeon dungeon) {
        return new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.PAPER).setDisplayName(dungeon.getName()).addLores(Main.getPlugin(Main.class).getDungeonManager().getDunLore(dungeon)).Create();
            }
        }.onInteract((player, clickType) -> {
            if(clickType.isLeftClick()){
                new EditUI(Main.getPlugin(Main.class), dungeon).open(player);
            }

        });
    }

}
