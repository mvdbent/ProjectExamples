package com.mortalpvp.mpkingdomdungeon.ui;

import com.mortalpvp.core.api.ui.UI;
import com.mortalpvp.core.api.ui.components.UIComponent;
import com.mortalpvp.core.bukkit.util.ItemCreator;
import com.mortalpvp.core.common.chat.C;
import com.mortalpvp.mpkingdomdungeon.Main;
import com.mortalpvp.mpkingdomdungeon.manager.dungeonmanager.Dungeon;
import com.mortalpvp.mpkingdomdungeon.manager.editmanager.EditPlayer;
import com.mortalpvp.mpkingdomdungeon.manager.editmanager.EditType;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

public class EditUI extends UI {
    public EditUI(Main plugin, Dungeon dungeon) {
        super(plugin, "Editing: &c" + dungeon.getName(), 27, InventoryType.CHEST);

        setComponent(10, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.FLOWER_POT_ITEM).setDisplayName("&cPort locations").addLores("&5Add port-locations with the WorldEdit selection").Create();
            }
        }.onInteract((player, clickType) -> {
                    if (clickType.isLeftClick()) {
                        player.closeInventory();
                        player.sendMessage(C.TAC("&cMake a region and type &a'ok' in the chat"));
                        plugin.getDungeonManager().getInEditing().add(new EditPlayer(player.getUniqueId(), EditType.PORTLOCATIONS, dungeon));
                    }
                }));
        setComponent(11, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.EXP_BOTTLE).setDisplayName("&cStage locations").addLores("&5Add stage-locations with the WorldEdit selection").Create();
            }
        });
        setComponent(12, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.SKULL_ITEM).setDisplayName("&cMob spawnpoints").addLores("&5Add mob-spawnpoints with the location you are on").Create();
            }
        });

        setComponent(13, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.CHEST).setDisplayName("&cChest Items").addLores("&5Add items on the chest you clicked on").Create();
            }
        });
        setComponent(14, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.BREWING_STAND_ITEM).setDisplayName("&cBoss-Location").addLores("&5Sets the bos location on the players location").Create();
            }
        });

        setComponent(15, new UIComponent() {
            @Override
            public ItemStack getItemStack(Player player) {
                return new ItemCreator(Material.PAPER).setDisplayName("&cEdit basic-values").addLores("&5Edit the Name,XP and Worldname of the dungeon").Create();
            }
        });
    }


}
