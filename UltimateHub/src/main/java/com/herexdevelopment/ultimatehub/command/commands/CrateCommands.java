package com.herexdevelopment.ultimatehub.command.commands;


import com.herexdevelopment.ultimatehub.lootbox.LootboxManager;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import com.herexdevelopment.ultimatehub.command.command.BasicCommand;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 30-4-18
 */
public class CrateCommands extends BasicCommand {


    public CrateCommands() {
        super("crate", "Main commands of crates", "utlimatehub.crate", true);
        addArgs("place", "utlimatehub.crate.place", "place - load and place a configuration", true);
    }


    @Override
    public void run(CommandSender sender, String[] args) {
        Player p = (Player) sender;
        if(args[0].equalsIgnoreCase("place") && args.length == 3){
            if(LootboxManager.getInstance().getLootbox(args[1]) == null){
                sender.sendMessage("Lootbox dont exist");
            }else{
                LootboxManager.getInstance().placeLootbox(p, args[1]);
                LootboxManager.getInstance().getLootbox(args[1]).getLocs().add(p.getLocation().add(0,1,0));
            }
        }
    }
}
