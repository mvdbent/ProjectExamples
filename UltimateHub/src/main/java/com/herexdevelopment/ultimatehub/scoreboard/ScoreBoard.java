package com.herexdevelopment.ultimatehub.scoreboard;

import com.herexdevelopment.ultimatehub.menu.MenuDataManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;
import org.json.JSONObject;
import com.herexdevelopment.ultimatehub.UltimateHub;
import com.herexdevelopment.ultimatehub.configmanager.ConfigManager;
import com.herexdevelopment.ultimatehub.configmanager.json.JsonConfig;
import com.herexdevelopment.ultimatehub.configmanager.json.JsonConfigManager;
import com.herexdevelopment.ultimatehub.utils.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 29-4-18
 */
public class ScoreBoard {
    private Set<String> keys;
    private ScoreboardManager manager = Bukkit.getScoreboardManager();
    private Scoreboard board = manager.getNewScoreboard();
    private Objective obj;
    private int index = 0;
    private List<String> list = new ArrayList<>();
    private static ScoreBoard instance = null;
    private String TITLE;



    public ScoreBoard() throws IllegalStateException {
        if (instance != null)
            throw new IllegalStateException("Only one instance can run");
        if (ConfigManager.getInstance().getConfig("config").getConfiguration().getBoolean("scoreboard")) {
            TITLE = StringUtils.repColor(ConfigManager.getInstance().getConfig("config").getConfiguration().getString("scoreboard_data.title"));
            JsonConfig config = JsonConfigManager.getInstance().getJsonByName("scoreboard");
            config.setFilename("scoreboard.json");
            config.setFolder(UltimateHub.getInstance().getDataFolder());
            config.setFile(new File(config.getFolder(), config.getFilename()));
            JSONObject objs = config.getJsonObject("scoreboard").getJSONObject("scores");
            setKeys(objs.keySet());
            obj = board.registerNewObjective(TITLE, "dummy");
            obj.setDisplaySlot(DisplaySlot.SIDEBAR);
            runBoard();
            loadAnimation();
        }
        instance = this;
    }

    public static ScoreBoard getInstance() {
        if (instance == null)
            throw new IllegalStateException("Cannot get instance: instance is null");
        return instance;
    }


    public void setBoard(Player p) {
        if (ConfigManager.getInstance().getConfig("config").getConfiguration().getBoolean("scoreboard")) {
            JsonConfig config = JsonConfigManager.getInstance().getJsonByName("scoreboard");
            config.setFilename("scoreboard.json");
            config.setFolder(UltimateHub.getInstance().getDataFolder());
            config.setFile(new File(config.getFolder(), config.getFilename()));
            JSONObject objs = config.getJsonObject("scoreboard").getJSONObject("scores");
            for (String k : getKeys()) {
                String name = objs.getJSONObject(k).getString("scorename");
                if (name.contains(";")) {
                    if (board.getTeam(k) == null) {
                        String[] splitter = name.split(";");
                        Team s = this.board.registerNewTeam(k);
                        s.addEntry(StringUtils.repColor(splitter[0]));
                        s.setSuffix(StringUtils.repvar(p, splitter[1]));
                        obj.getScore(StringUtils.repColor(splitter[0])).setScore(objs.getJSONObject(k).getInt("scorepos"));
                    } else {
                        String[] splitter = name.split(";");
                        Team s = board.getTeam(k);
                        s.addEntry(StringUtils.repColor(splitter[0]));
                        s.setSuffix(StringUtils.repvar(p, splitter[1]));
                        obj.getScore(StringUtils.repColor(splitter[0])).setScore(objs.getJSONObject(k).getInt("scorepos"));
                    }
                } else {
                    Score s = obj.getScore(StringUtils.repColor(objs.getJSONObject(k).getString("scorename")));
                    s.setScore(objs.getJSONObject(k).getInt("scorepos"));
                }
            }
            p.setScoreboard(board);
        }else{
            return;
        }

    }
    private void loadAnimation() {
        if (ConfigManager.getInstance().getConfig("config").getConfiguration().getBoolean("scoreboard_animations")) {
                for (String item: ConfigManager.getInstance().getConfig("config").getConfiguration().getStringList("scoreboard_data.animated_title")) {
                    getList().add(item);
                }
            }
        }

    private void updateBoard(Player p) {
        JsonConfig config = JsonConfigManager.getInstance().getJsonByName("scoreboard");
        config.setFilename("scoreboard.json");
        config.setFolder(UltimateHub.getInstance().getDataFolder());
        config.setFile(new File(config.getFolder(), config.getFilename()));
        JSONObject objs = config.getJsonObject("scoreboard").getJSONObject("scores");
        this.keys = objs.keySet();
        for (String k : getKeys()) {
            String name = objs.getJSONObject(k).getString("scorename");
            if (name.contains(";")) {
                String[] splitter = objs.getJSONObject(k).getString("scorename").split(";");
                if (board.getTeam(k) == null) {
                    Team t = board.registerNewTeam(k);
                    t.setSuffix(StringUtils.repvar(p, splitter[1]));
                } else {
                    Team t = board.getTeam(k);
                    t.setSuffix(StringUtils.repvar(p, splitter[1]));
                }
            }
        }
    }

    private void runBoard() {
        Bukkit.getScheduler().runTaskTimerAsynchronously(UltimateHub.getInstance(), () -> {
            if (!Bukkit.getOnlinePlayers().isEmpty()) {
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    if (pl.getScoreboard() != null) {
                        updateBoard(pl);
                    } else {
                        setBoard(pl);
                    }
                    if (list.isEmpty()) {
                        return;
                    }
                    if (index == list.size()) {
                        index = 0;
                    }
                    if (ConfigManager.getInstance().getConfig("config").getConfiguration().getBoolean("scoreboard_animations")) {
                        pl.getScoreboard().getObjective(TITLE).setDisplayName(StringUtils.repColor(list.get(index)));
                        index++;

                    }
                }

            }

        }, 10, 10);
    }


    public Set<String> getKeys() {
        return keys;
    }

    public void setKeys(Set<String> keys) {
        this.keys = keys;
    }

    public ScoreboardManager getManager() {
        return manager;
    }

    public void setManager(ScoreboardManager manager) {
        this.manager = manager;
    }

    public Scoreboard getBoard() {
        return board;
    }

    public void setBoard(Scoreboard board) {
        this.board = board;
    }

    public Objective getObj() {
        return obj;
    }

    public void setObj(Objective obj) {
        this.obj = obj;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public List<String> getList() {
        return list;
    }

    public void setList(ArrayList<String> list) {
        this.list = list;
    }

    public void recapBoard(){
        if (ConfigManager.getInstance().getConfig("config").getConfiguration().getBoolean("scoreboard")) {
            for (Player p : Bukkit.getOnlinePlayers()) {
                p.getScoreboard().clearSlot(DisplaySlot.SIDEBAR);
                setBoard(p);
            }
        }

    }
}
