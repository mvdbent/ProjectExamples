package com.herexdevelopment.ultimatehub.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import com.herexdevelopment.ultimatehub.joindata.JoinItem;
import com.herexdevelopment.ultimatehub.menu.ExecuteEvent;
import com.herexdevelopment.ultimatehub.menu.InvItem;
import com.herexdevelopment.ultimatehub.menu.MenuDataManager;
import com.herexdevelopment.ultimatehub.menu.UtlimateMenu;
import com.herexdevelopment.ultimatehub.utils.ItemBuilder;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 29-4-18
 */
public class MenuListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {

        if (e.getPlayer().getItemInHand() == null || e.getPlayer().getItemInHand().getType() == Material.AIR) {
            return;
        }

        if(e.getPlayer().getInventory().getType().equals(InventoryType.CREATIVE)){
            return;
        }


        for (JoinItem join : MenuDataManager.getInstance().getJoinitems()) {
            if (e.getPlayer().getInventory().getItemInMainHand().isSimilar(new ItemBuilder(Material.getMaterial(join.getItemid())).name(join.getDisplayname()).amount(1).lore(join.getLore()).build()) && e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                if(e.getPlayer().getItemInHand().getTypeId() != join.getItemid()){
                    return;
                }
                    UtlimateMenu menu = MenuDataManager.getInstance().getMenu(join.getMenuname());
                    Inventory inv = Bukkit.createInventory(null, menu.getSize(), menu.getDisplayname());
                    for (InvItem item : menu.getInvItems()) {
                        inv.setItem(item.getSlotId(), new ItemBuilder(Material.getMaterial(item.getId())).amount(1).name(item.getDisplayname()).lore(item.getLore()).build());
                    }
                    e.getPlayer().openInventory(inv);
                }
            }

    }


    @EventHandler
    public void onInvClick(InventoryClickEvent e) {
        if (e.getWhoClicked().getType().equals(EntityType.PLAYER)) {
            if (e.getCurrentItem().isSimilar(new ItemStack(Material.AIR)) || e.getCurrentItem() == null) {
                return;

            } else {
                for (JoinItem join : MenuDataManager.getInstance().getJoinitems()) {
                    UtlimateMenu menu = MenuDataManager.getInstance().getMenu(join.getMenuname());
                    for (InvItem item : menu.getInvItems()) {
                        if (e.getCurrentItem().isSimilar(new ItemBuilder(Material.getMaterial(item.getId())).amount(1).name(item.getDisplayname()).lore(item.getLore()).build())) {
                            if (!item.getEvent().equalsIgnoreCase("")) {
                                Player p = (Player) e.getWhoClicked();
                                ExecuteEvent.getInstance().execute(p, item.getEvent());
                                break;
                            }
                        }
                    }

                }

            }
        } else {
            return;
        }
    }
}
