package com.herexdevelopment.ultimatehub.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import com.herexdevelopment.ultimatehub.configmanager.ConfigManager;
import com.herexdevelopment.ultimatehub.lootbox.InterfaceItem;
import com.herexdevelopment.ultimatehub.lootbox.LootBox;
import com.herexdevelopment.ultimatehub.lootbox.LootboxManager;
import com.herexdevelopment.ultimatehub.menu.ExecuteEvent;
import com.herexdevelopment.ultimatehub.utils.ItemBuilder;
import com.herexdevelopment.ultimatehub.utils.StringUtils;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 30-4-18
 */
public class CrateListener implements Listener {


    @EventHandler
    public void onInteract(PlayerInteractEvent e){


        if(e.getPlayer().getInventory().getType().equals(InventoryType.CREATIVE)){
            return;
        }
        if(ConfigManager.getInstance().getConfig("data").getConfiguration().getConfigurationSection("crates") == null){
            return;
        }

        if(LootboxManager.getInstance().getLootBoxes().isEmpty()){
            return;
        }


        for(String key : ConfigManager.getInstance().getConfig("data").getConfiguration().getConfigurationSection("crates").getKeys(false)) {
            double x = ConfigManager.getInstance().getConfig("data").getConfiguration().getDouble("crates." + key + ".locx");
            double y = ConfigManager.getInstance().getConfig("data").getConfiguration().getDouble("crates." + key + ".locy");
            double z = ConfigManager.getInstance().getConfig("data").getConfiguration().getDouble("crates." + key + ".locz");
                for (LootBox b : LootboxManager.getInstance().getLootBoxes()) {

                    if(e.getClickedBlock() == null){
                        return;
                    }
                    if (e.getClickedBlock().getLocation().equals(new Location(e.getPlayer().getWorld(), x, y, z)) && e.getClickedBlock().getTypeId() == b.getCrateBlockId() && e.getClickedBlock().getType() != Material.AIR) {
                        Inventory inv = Bukkit.createInventory(null, b.getInvSize(), b.getCrateName());
                        for (InterfaceItem i : b.getInterfaceItems()) {
                            inv.setItem(i.getSlotID(), new ItemBuilder(Material.getMaterial(i.getId())).name(StringUtils.repColor(i.getDisplayname())).build());
                        }
                        e.getPlayer().openInventory(inv);
                        break;
                    }else{
                        return;
                    }
                    }
            }
        }



    @EventHandler
    public void onInvClick(InventoryClickEvent e) {


        if(e.getCurrentItem() == null){
            return;
        }
        if (e.getWhoClicked().getType().equals(EntityType.PLAYER)) {
            if (e.getCurrentItem().isSimilar(new ItemStack(Material.AIR)) || e.getCurrentItem() == null || e.getClickedInventory().getType() == InventoryType.CREATIVE) {
                return;

            } else {
                LootBox menu = LootboxManager.getInstance().getLootbox(e.getClickedInventory().getTitle());
                for (InterfaceItem item : menu.getInterfaceItems()) {
                    if (e.getCurrentItem().isSimilar(new ItemBuilder(Material.getMaterial(item.getId())).amount(1).name(StringUtils.repColor(item.getDisplayname())).build())) {
                        if (!item.getEvent().equalsIgnoreCase("")) {
                            Player p = (Player) e.getWhoClicked();
                            ExecuteEvent.getInstance().execute(menu, p, item.getEvent());
                            break;
                        }
                    }
                }
            }


        }
        }



        }