package com.herexdevelopment.ultimatehub.particle;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 2-5-18
 */
public class ParticleDesign {
    private String name;
    private float x,y,z;
    private float speed;
    private int amount;


    public ParticleDesign(String name, float x, float y, float z, float speed, int amount) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.z = z;
        this.speed = speed;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double getSpeed() {
        return speed;
    }

    public int getAmount() {
        return amount;
    }
}
