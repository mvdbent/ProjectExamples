package com.herexdevelopment.ultimatehub.menu;

import com.herexdevelopment.ultimatehub.joindata.JoinItem;
import com.herexdevelopment.ultimatehub.lootbox.LootboxManager;
import com.herexdevelopment.ultimatehub.utils.StringUtils;
import org.bukkit.configuration.file.FileConfiguration;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.herexdevelopment.ultimatehub.UltimateHub;
import com.herexdevelopment.ultimatehub.configmanager.ConfigManager;


import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 28-4-18
 */
public class MenuDataManager {
    public ArrayList<UtlimateMenu> menus = new ArrayList<>();
    public ArrayList<JoinItem> joinitems = new ArrayList<>();
    HashMap<String, File> configs = new HashMap<>();
    private static MenuDataManager instance = null;
    public MenuDataManager() throws IllegalStateException {
        if (instance != null)
            throw new IllegalStateException("Only one instance can run");
        instance = this;
    }

    public static MenuDataManager getInstance() {
        if (instance == null)
            throw new IllegalStateException("Cannot get instance: instance is null");
        return instance;
    }

    public void loadMenu() throws IOException, ParseException {
        for(Map.Entry<String, File> ents:  configs.entrySet()) {
            if (ents.getKey().contains("menu_")) {
                JSONParser parser = new JSONParser();
                    String json = parser.parse(new FileReader(ents.getValue())).toString();
                JSONObject obs = new JSONObject(json);

                JSONObject objss = obs.getJSONObject("items");
                JSONObject objs = obs.getJSONObject("settings");
                UtlimateMenu menu = new UtlimateMenu(objs.getString("menuname"), objs.getString("menu_name"), objs.getInt("menu_size"), objs.getInt("menusound_id"), new ArrayList<InvItem>());
                Set<String> keys = objss.keySet();
                for (String k : keys) {
                    InvItem item = new InvItem(objss.getJSONObject(k).getInt("id"), objss.getJSONObject(k).getInt("slotid"), objss.getJSONObject(k).getInt("click_sound_id"), objss.getJSONObject(k).getString("displayname"), objss.getJSONObject(k).getString("event"), new ArrayList<String>());
                    JSONArray lores = objss.getJSONObject(k).getJSONArray("lore");
                    for (int i = 0; i < lores.length(); ++i) {
                        item.getLore().add(StringUtils.repColor(lores.get(i).toString()));
                    }

                    menu.getInvItems().add(item);

                }

                menus.add(menu);
                }

        }
    }


    public void loadJsonFiles(){
        System.out.print(UltimateHub.getInstance().getDataFolder().getAbsolutePath());
        File dir = new File(UltimateHub.getInstance().getDataFolder().getAbsolutePath() + "/menus/");
        File[] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".json");
            }
        });
        for(int i = 0; i < files.length; i++){
            if(files[i].getName().contains("menu")){
                configs.put(files[i].getName(), files[i]);
            }
        }



    }

    public void loadJoinItems(){
        for(String key : ConfigManager.getInstance().getConfig("items").getConfiguration().getConfigurationSection("items.hub").getKeys(false)){
            FileConfiguration conf = ConfigManager.getInstance().getConfig("items").getConfiguration();
            int itemid = conf.getInt("items.hub." + key + ".itemid");
            String displayname = conf.getString("items.hub." + key + ".displayname");
            int soundfx = conf.getInt("items.hub." + key + ".sound_effect");
            String menuname = conf.getString("items.hub." + key + ".menu_name");
            int slot_id = conf.getInt("items.hub." + key + ".slot_id");
            JoinItem item = new JoinItem(itemid, slot_id, soundfx,displayname,menuname, new ArrayList<String>());
            for(String k : conf.getStringList("items.hub." + key + ".lore")){
                item.getLore().add(k);
            }
            joinitems.add(item);



        }

    }

    public UtlimateMenu getMenu(String name){
        for(UtlimateMenu menu : menus){
            if(menu.getTitle().endsWith(name)){
                return menu;
            }
        }

        return null;

    }
    public ArrayList<JoinItem> getJoinitems() {
        return joinitems;
    }
}
