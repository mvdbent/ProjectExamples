package com.herexdevelopment.ultimatehub.menu;

import java.util.List;

/**
 * Created by: Naomi Beerenfenger
 * Project: UltimateHub
 * Creation date: 28-4-18
 */
public class InvItem {
    private int id, slotId, clickSoundId;
    private String displayname, event;
    private List<String> lore;


    public InvItem(int id, int slotId, int clickSoundId, String displayname, String event, List<String> lore) {
        this.id = id;
        this.slotId = slotId;
        this.clickSoundId = clickSoundId;
        this.displayname = displayname;
        this.event = event;
        this.lore = lore;
    }

    public List<String> getLore() {
        return lore;
    }

    public int getId() {
        return id;
    }

    public int getSlotId() {
        return slotId;
    }

    public int getClickSoundId() {
        return clickSoundId;
    }

    public String getDisplayname() {
        return displayname;
    }

    public String getEvent() {
        return event;
    }
}
